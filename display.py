# Authors: David Snider and Nathan Immerman

import importFile
import delta

def displayHeader():
    """ Displays the header line for the table.

    Parameters: None
    Requires: Nothing
    Modifies: Nothing
    Effects: Display a header line: width 35 for the country name (left
             justified), 15 each for the population and density (both
             right justified).
    Text: 'Country/Principality', 'Population', 'Density'
    """
    print('Country/Principality                    Population        Density')

def histogramLine(name, dens):
    """ Creates a histogram line for a given country and population density.

    Parameters: name: The name of the country
                dens: The population density of the country
    Requires: dens >= 0; population data loaded and advanced in years
    Modifies: Nothing
    Effects: Creates and returns a line of text, containing a country name
             (right justified, width 35), then ': ', finally a number of
             asterisks equal to the population density / 1000, rounded.
    """
    line = name.rjust(35) + ':'
    for x in xrange(0, int(round((dens / 1000)))):
        line += '*'
    return line

def makeHistogram(data):
    """ Creates the histogram for the top 10 most population-dense countries.

    Parameters: data: A list of lists; each row of the list represents
                one country
    Requires: Population data loaded, advanced in years, and sorted
              by density
    Modifies: Nothing
    Effects: Take the 10 most populous countries in the table, create a
             histogram line for each one, and display it.
    Text: 'Top countries by density:'
    """

    print('Top countries by density:')
    y = 10

    if len(data) < 10:
        y = len(data)

    for x in xrange(0,y):
        print(histogramLine(data[x].name, data[x].newDen))


def displayTable(data):
    """ Creates most of the output for the program in a formatted table.

    Parameters: data: A list of lists; each row of the list represents
                one country
    Requires: Population data loaded and advanced in years
    Modifies: Nothing
    Effects: Displays the population table, nicely formatted.  It should
             consist of a header line as described in displayHeader(),
             a line of dashes (length 65), the table with the country name
             (left justified, width 35), future population (right justified,
             width 15), and future density (same format as population).
             After the table, draw another line of dashes, followed by the
             total world population at that future year.

             NOTE: the population should be truncated to an integer (don't
             round .5 people up into a whole one), but the density should
             be rounded to the nearest integer.
    Text: 'Total population'
    """

    displayHeader()
    print('-' * 65)

    pop = 0
    
    for country in data:
        pop += int(country.newPop)
        line_new = country.name.ljust(35) + str(int(country.newPop)).rjust(15) + str(int(round(country.newDen))).rjust(15)
        print line_new

    print('-' * 65)
    line_new = 'Total population'.ljust(35) + str(pop).rjust(15) + ''.rjust(15)
    print(line_new)
    print('')
    print('')

if __name__ == '__main__':
    line = ['France', 2, 6, 3]
    line2 = ['Germany', 1.1, 5, 7]
    c = importFile.Country(line)
    d = importFile.Country(line2)
    data = [c,d]
    delta.advanceYears(data, 3)
    displayTable(data)
    makeHistogram(data)

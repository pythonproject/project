test:
	python importFile.py
	python popdens.py < tester.txt
clean:
	rm *.pyc *~
cache:
	git config credential.helper 'cache --timeout=3600'

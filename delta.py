# Authors: David Snider and Nathan Immerman

import importFile
    
def advanceYears(data, years):
    """ Predicts each country's population and density in the future.

    Parameters: data: A list of lists; each row of the list represents
                one country
                years: the number of years to predict population growth
    Requires: years > 0; population data loaded
    Modifies: data
    Effects: For each row, append two new floating-point numbers to it:
               the new population after the years have passed
               the future population density (new population / area)
    """

    for country in data:
        country.newPop = country.curPop * pow((1 + country.popGrowth), years)
        country.newDen = country.newPop / country.area

def sortByDensity(data):
    """ Sorts the data list of lists by density, highest to lowest.

    Parameters: data: A list of lists; each row of the list represents
                one country
    Requires: Population data loaded and advanced in years
    Modifies: data
    Effects: Sort data by population density (column 5), from highest
             to lowest
    """

    data.sort(key=lambda Country: Country.newDen, reverse=True)

if __name__ == '__main__':
    line = ['France', 2, 6, 3]
    line2 = ['Germany', 1.1, 5, 7]
    c = importFile.Country(line)
    d = importFile.Country(line2)
    data = [c,d]
    advanceYears(data, 3)
    print(c.name, d.name)
    print(c.curPop, d.curPop)
    print(c.newPop, d.newPop)
    print(data[0].newDen, data[1].newDen)
    sortByDensity(data)
    print(data[0].newDen, data[1].newDen)


    #whoohoo its a comment
    #whoohoo its a comment

# Authors: David Snider and Nathan Immerman

import importFile
import sys
import delta
import display

def main(argv):
    """ put your code for the main program below this line

    When you get the filename, just pass it to processInputFile().
    If that file returns a list of length 0, do nothing.

    When you ask for the number of years, if it is not > 0, display
    an error message, prompt them again, etc. until they enter a
    correct number of years.

    Prompts: 'Enter filename to open: '
             'Enter number of years for population to grow: '
             'Number of years must be > 0'
    """
    filename = raw_input ("Enter filename to open: ")
#    print filename
    data = importFile.processInputFile(filename)
    if len(data) == 0:
        print "you done fucked up, son"
        return
    years = raw_input ('Enter number of years for population to grow: ')
 #   print years
    delta.advanceYears(data, int(years))
    delta.sortByDensity(data)
    display.displayTable(data)
    display.makeHistogram(data)

if __name__ == "__main__":
    main(sys.argv)

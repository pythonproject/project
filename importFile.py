# Authors: David Snider and Nathan Immerman

import sys

class Country:
    """A container for the information in each of the input lines"""
    def __init__(self, line):
        """parses the line and puts it in the required variables"""
        tempList = line.split(",")
        self.name = tempList[0]
        self.area = float(tempList[1])
        self.curPop = float(tempList[2])
        self.popGrowth = float(tempList[3]) / 100
        self.newPop = 0
        self.newDen = 0


def processInputLine(data, line):
    """ Converts a line of input to a list and appends it to the data.

    Parameters: data: A list of classes; each object of the list represents
                one country
                line: one new line of input from the file
    Requires: line has 4 pieces of information separated by commas
              (name of country, area, current population, population growth)
    Modifies: data
    Effects: A new object is added to data; the object will contain 4 elements.
             The line must be split apart into elements (they are currently 
             separated by commas).  The name of the country is added as a 
             string; the other three values (area, current population, 
             growth rate) are converted to float before being added.

             NOTE: If the line is blank (which should not happen, even at
			 the end of the file), take no action.  If the first character
                         of the line is #, it is a comment and should not be added to
			 data.
    """
    if not line or line[0] == '#':
        return
    else:
        data.append(Country(line))


def processInputFile(filename):
    """ Open input file, processing every line to produce a list of classes.

    Parameters: filename: The name of a file to load from disk.
    Requires: Nothing
    Modifies: Nothing
    Effects: Starts with an empty list, opens the input file, and then
             for each line of the file it calls processInputLine().
             When the function ends, it returns the list of  classes that
             has been built.

             If the file cannot be opened, display an error message
             and return an empty list.
    Text: 'File failed to open'
    """
    data = []
    try:
        f = open (filename, 'r')
    except:
        print "File failed to open"
        return data
    for line in f:
        processInputLine(data, line)
    f.close()
    return data


if __name__ == "__main__":
    data = []
    line = "herp,5,457,6532"
    processInputLine(data, line)
    print "should be:   herp, 5, 457, 65"
    print "is actually: %s, %d, %d, %d" %(data[0].name, data[0].area, data[0].curPop, data[0].popGrowth) 
    assert "herp, 5, 457, 65" == "%s, %d, %d, %d" %(data[0].name, data[0].area, data[0].curPop, data[0].popGrowth) 

    line = "France,210026,63929000,1.15"
    processInputLine(data, line)
    print "should be:   France, 210026, 63929000, 0.011500"
    print "is actually: %s, %d, %d, %f" %(data[1].name, data[1].area, data[1].curPop, data[1].popGrowth) 
    assert "France, 210026, 63929000, 0.011500" == "%s, %d, %d, %f" %(data[1].name, data[1].area, data[1].curPop, data[1].popGrowth) 

    line = ""
    processInputLine(data, line)
    assert len(data) == 2

    data = []
    print "\nfile failure test:"
    data = processInputFile('herp.derp')
    assert (len(data) == 0)

    data = processInputFile('pop-small.csv')
    
    print "\nwhat we've pulled:"

    for x in data:
        print "%s, %f, %f, %f" %(x.name, x.area, x.curPop, x.popGrowth) 

    print "\nwhat we were given:"
    import os
    os.system("cat pop-small.csv")    
    print ""
